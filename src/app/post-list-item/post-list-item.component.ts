import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-post-list-item',
  templateUrl: './post-list-item.component.html',
  styleUrls: ['./post-list-item.component.scss']
})
export class PostListItemComponent implements OnInit {

  @Input() title: string;
  @Input() content: string;
  @Input() loveIts: number;
  @Input() created_at: Object;

  constructor() { }

  ngOnInit() {
  }

  /**
   * @description augmenter le nombre de loveIts
   */
  increaseLoves() {
    this.loveIts++;
  }

  /**
   * @description diminuer le nombre de loveIts
   */
  decreaseLoves() {
    this.loveIts--;
  }
}
